// ============================================
//
//           CS542 Project
//
// --------------------------------------------
// filename : fft_base.c
// author   : Gates Wong
// date     : 2014-05-06
//
// This project entails implementing
// two-dimensional convolution on parallel
// computers using different parallelization
// techniques and models.
//
// This file is the origin version of FFT
//
// Origin version is designed for Linear
// processing models. This is non-parallel
// program.
//
// ============================================

#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "fft_base.h"


// c_fft1d() function
// parameters:
//      complex *r : real and imaginary arrays of n = 2^m points
//      int isign : -1 gives forward transform
//                  1 gives inverse transform
void c_fft1d(complex *r, int n, int isign) {
	int m, i, i1, j, k, i2, l, l1, l2;
	float c1, c2, z;
	complex t, u;

	if (0 == isign) { return; }

	// Bit reversals
	i2 = n >> 1; j = 0;
	for (i = 0; i < n - 1; i ++) {
		if (i < j) { C_SWAP(r[i], r[j]); }
		k = i2;
		while (k <= j) { j -= k; k >>= 1; }
		j += k;
	}

	for (i = n, m = 0; i > 1; m ++, i /= 2);

	// Compute FFT
	c1 = -1.0; c2 = 0.0; l2 = 1;
	for (l = 0; l < m; l ++) {
		l1 = l2; l2 <<= 1;
		u.r = 1.0; u.i = 0.0;

		for (j = 0; j < l1; j ++) {
			for (i = j; i < n; i += l2) {
				i1 = i + l1;

				// t = u * r[i1]
				t.r = u.r * r[i1].r - u.i * r[i1].i;
				t.i = u.r * r[i1].i + u.i * r[i1].r;

				// r[i1] = r[i] - t
				r[i1].r = r[i].r - t.r;
				r[i1].i = r[i].i - t.i;

				// r[i] = r[i] + t
				r[i].r += t.r; r[i].i += t.i;
			}

			z = u.r * c1 - u.i * c2;
			u.i = u.r * c2 + u.i * c1;
			u.r = z;
		}

		c2 = sqrt((1.0 - c1) / 2.0);
		if (-1 == isign) { c2 = -c2; }
		c1 = sqrt((1.0 + c1) / 2.0);
	}

	// Scaling for inverse transform
	if (1 == isign) {		// inversed FFT
		for (i = 0; i < n; i ++) {
			r[i].r /= n;
			r[i].i /= n;
		}
	}
}

// matrix_transpose() function
// parameters:
//     complex* data : the matrix used for transform
//     rowstart : row index of start point
//     colstart : col index of start point
//     size : the size of transpose part
void matrix_transpose(complex (*data)[512], int rowstart, int colstart, int size) {
	int row, col, ii, jj;
	complex tmp;

	for (row = 0; row < size; row ++) {
		for (col = row + 1; col < size; col ++) {
			tmp = data[rowstart + row][colstart + col];
			data[rowstart + row][colstart + col] = data[rowstart + col][colstart + row];
			data[rowstart + col][colstart + row] = tmp;
        }
	}
}

// matrix_blockcopy() function
//     copy a block of matrix
// parameters:
//     src: the source matrix
//     dst: the destination matrix
//     srow, scol: where source matrix starts
//     trow, tcol: where destination matrix starts
//     size: how large is the block
void matrix_blockcopy(complex (* src)[512], complex (*dst)[512], int srow, int scol,
	int trow, int tcol, int size) {
	int orow, ocol;		// offsets
	for (orow = 0; orow < size; orow ++) {
		for (ocol = 0; ocol < size; ocol ++) {
			dst[trow + orow][tcol + ocol] = src[srow + orow][scol + ocol];
		}
	}
}

// ReadIm() function
// parameters:
//     filepath - The full path of input file.
//     data - the pointer where data store at.
// return:
//     values in FileOperationResults.
int ReadIm(char* filepath, complex (*data)[512]) {
	FILE *fp;
	int ii, jj;

	if (!(fp = fopen(filepath, "r"))) { return IM_FILE_CANNOT_OPEN; }

	for (ii = 0; ii < 512; ii ++) {
		for (jj = 0; jj < 512; jj ++) {
			fscanf(fp, "%g", &(data[ii][jj].r));
			data[ii][jj].i = 0.0;
		}
	}

	return IM_FILE_SUCCESS;
}


// WriteIm() function
// parameters:
//     filepath - The full path of input file.
//     data - the pointer where data store at.
// return:
//     values in FileOperationResults.
int WriteIm(char* filepath, complex (*data)[512]) {
	FILE *fp;
	int ii, jj;

	if (!(fp = fopen(filepath, "w"))) { return IM_FILE_CANNOT_OPEN; }

	for (ii = 0; ii < 512; ii ++) {
		for (jj = 0; jj < 512; jj ++) {
			fprintf(fp, "   %11.5g", data[ii][jj].r);
		}
		fprintf(fp, "\n");
	}

	return IM_FILE_SUCCESS;
}
