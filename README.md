# README

## HOW TO COMPILE

For each problem, there is a single `.c` file named `a.c`, `b.c`, `c.c` and `d.c`.
There exists the compilation code in project folder, they are named as `ca.sh`, `cb.sh`, `cc.sh` and `cd.sh`.

If you want to compile these code by yourself, you should use the following command (e.g problem a):

	mpicc -c a.c fft_base.c

	mpicc -o a a.o fft_base.o

Then the file `a` is the final executable file.

## HOW TO RUN

If you use the compile script provided, you will find the executable files in `eX` (which X is a, b, c or d).

In that folder, you will find some scripts named `sXN.bash` (N means number of processes) means submit the job.

## INPUT FILE

Make sure that you put input file the same folder as the executable files, or you can try to put them in
the $HOME folder.

## OUTPUT FILE

You can find the output file by qsub at $HOME or the subfolder named `outN` where the executable file is.

You can find the result file in the same folder as the executable files.
