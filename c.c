// ============================================
//
//           CS542 Project
//
// --------------------------------------------
// filename : c.c
// author   : Gates Wong
// date     : 2014-05-06
//
// This project entails implementing
// two-dimensional convolution on parallel
// computers using different parallelization
// techniques and models.
//
// This file is the origin version of FFT
//
// FFT using mixed of MPI and pthread.
//
// ============================================

#include "fft_base.h"

#include <stdio.h>
#include <mpi.h>
#include <pthread.h>

//#define DEBUG
#ifdef DEBUG
char debug_file[1000];
int debug_count;
#endif

// timer utility
double t_comm, t_comp, t_last;
void tclear() { t_comm = t_comp = 0.0; }
void tic() { t_last = MPI_Wtime(); }
void tac(double* t) { *t += MPI_Wtime() - t_last; }

// global MPI Variables
int rank, nproc;	// current rank and number of processes
int b_size;
MPI_Datatype FFT_COMPLEX, FFT_BLOCK;
void MPI_Datatype_Init() {
	b_size = 512/nproc;
	MPI_Type_contiguous(2, MPI_FLOAT, &FFT_COMPLEX);
	MPI_Type_commit(&FFT_COMPLEX);			// MPI type for complex
	MPI_Type_vector(b_size, b_size, 512, FFT_COMPLEX, &FFT_BLOCK);
	MPI_Type_commit(&FFT_BLOCK);			// MPI type for block growing vertically.
}

// global pthread Variables
#define NTHREAD 8
pthread_t pool[NTHREAD - 1];	// NTHREAD threads, so we need NTHREAD-1 threads + main thread

pthread_mutex_t th_mutex;
pthread_cond_t th_cond;
int th_max = NTHREAD, th_arrived = 0, th_mainarrived = 0;

// suspend tags
#define MATRIX_NONE 0x0000
#define MATRIX_COMM_A1 0x0001
#define MATRIX_COMM_A2 0x0002
#define MATRIX_COMM_B1 0x0004
#define MATRIX_COMM_B2 0x0008
#define MATRIX_COMM_C1 0x0010
#define MATRIX_COMM_C2 0x0020
#define GATHER_DATA 0x0040

MPI_Status status;

// vectors
complex A[512][512], B[512][512], C[512][512], T[512][512];

// matrix_transpose_p()
//     parallel version of matrix_transpose()
// parameters:
//     tid: the id of thread
//     tcount: total number of threads
void matrix_transpose_p(complex (*data)[512], int rowstart, int colstart, int size, int tid, int tcount) {
	int row, col, ii, jj;
	complex tmp;

	// top part, RowStart1, RowEnd1
	// We divide row into small parts that all threads will do their own works
	// there is also a bottom part that each thread get part of work from top and
	// part of works from bottom. If a thread has more work in top part, it will
	// get less works in bottom part.
	int rs1 = tid * (size / 2 / tcount), re1 = (tid + 1) * (size / 2 / tcount);
	for (row = rs1; row < re1; row ++) {
		for (col = row + 1; col < size; col ++) {
			tmp = data[rowstart + row][colstart + col];
			data[rowstart + row][colstart + col] = data[rowstart + col][colstart + row];
			data[rowstart + col][colstart + row] = tmp;
		}
	}

	// bottom part, RowStart2, RowEnd2
	int rs2 = size - re1, re2 = size - rs1;
	for (row = rs2; row < re2; row ++) {
		for (col = row + 1; col < size; col ++) {
			tmp = data[rowstart + row][colstart + col];
			data[rowstart + row][colstart + col] = data[rowstart + col][colstart + row];
			data[rowstart + col][colstart + row] = tmp;
		}
	}
}

// matrix_blockcopy_p()
//     parallel version of matrix_blockcopy()
// parameters:
//     tid: the id of thread
//     tcount: total number of threads
void matrix_blockcopy_p(complex (* src)[512], complex (*dst)[512], int srow, int scol,
	int trow, int tcol, int size, int tid, int tcount) {
	int orow, ocol;		// offsets
	int rs = tid * (size / tcount), re = (tid + 1) * (size / tcount);
	for (orow = rs; orow < re; orow ++) {
		for (ocol = 0; ocol < size; ocol ++) {
			dst[trow + orow][tcol + ocol] = src[srow + orow][scol + ocol];
		}
	}
}

// pthread functions
int destroysync() {
	pthread_mutex_destroy(&th_mutex);
	pthread_cond_destroy(&th_cond);
}

// suspend thread
// parameters:
//     tag: specify which operation should done by this work.
int suspend(int tag) {
	pthread_mutex_lock(&th_mutex);
	{
		th_arrived ++;
		if (th_arrived < th_max) {
			pthread_cond_wait(&th_cond, &th_mutex);
		} else {
			tac(&t_comp);
			th_arrived = 0;

			int p;
			if (nproc > 1) { tic(); switch(tag) {
				case MATRIX_COMM_A1:
					for (p = 1; p < nproc; p ++) {
						int s = (rank + p) % nproc,
							r = (rank - p + nproc) % nproc;
						MPI_Sendrecv(&A[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
							&T[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
							MPI_COMM_WORLD, &status);
					}
					break;
				case MATRIX_COMM_A2:
					for (p = 1; p < nproc; p ++) {
						int s = (rank + p) % nproc,
							r = (rank - p + nproc) % nproc;
						MPI_Sendrecv(&T[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
							&A[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
							MPI_COMM_WORLD, &status);
					}
					break;
				case MATRIX_COMM_B1:
					for (p = 1; p < nproc; p ++) {
						int s = (rank + p) % nproc,
							r = (rank - p + nproc) % nproc;
						MPI_Sendrecv(&B[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
							&T[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
							MPI_COMM_WORLD, &status);
					}
					break;
				case MATRIX_COMM_B2:
					for (p = 1; p < nproc; p ++) {
						int s = (rank + p) % nproc,
							r = (rank - p + nproc) % nproc;
						MPI_Sendrecv(&T[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
							&B[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
							MPI_COMM_WORLD, &status);
					}
					break;
				case MATRIX_COMM_C1:
					for (p = 1; p < nproc; p ++) {
						int s = (rank + p) % nproc,
							r = (rank - p + nproc) % nproc;
						MPI_Sendrecv(&C[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
							&T[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
							MPI_COMM_WORLD, &status);
					}
					break;
				case MATRIX_COMM_C2:
					for (p = 1; p < nproc; p ++) {
						int s = (rank + p) % nproc,
							r = (rank - p + nproc) % nproc;
						MPI_Sendrecv(&T[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
							&C[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
							MPI_COMM_WORLD, &status);
					}
					break;
				case GATHER_DATA:
					if (0 == rank) {
						tic();
						int p, q;
						for (p = 1; p < nproc; p ++) {
							// receive data from processor:
							MPI_Recv(C[p * b_size], 512 * b_size, FFT_COMPLEX, p, p, MPI_COMM_WORLD, &status);
						}
						tac(&t_comm);
					} else {
						tic();
						MPI_Send(C[rank * b_size], 512 * b_size, FFT_COMPLEX, 0, rank, MPI_COMM_WORLD);
						tac(&t_comm);
					}
					break;
				default: break;
			} tac(&t_comm); }
			pthread_cond_broadcast(&th_cond);
			tic();
		}
	}
	pthread_mutex_unlock(&th_mutex);
}

// Swap two complex numbers
static complex __ctmp_p[NTHREAD];
#define C_SWAPP(A, B, T) { __ctmp_p[T] = (A); (A) = (B); (B) = __ctmp_p[T]; }

// c_fft1d_p() function
//      parallel version of c_fft1d()
// parameters:
//      complex *r : real and imaginary arrays of n = 2^m points
//      int isign : -1 gives forward transform
//                  1 gives inverse transform
//      tid : the id of current thread
void c_fft1d_p(complex *r, int n, int isign, int tid) {
	int m, i, i1, j, k, i2, l, l1, l2;
	float c1, c2, z;
	complex t, u;

	if (0 == isign) { return; }

	// Bit reversals
	i2 = n >> 1; j = 0;
	for (i = 0; i < n - 1; i ++) {
		if (i < j) { C_SWAPP(r[i], r[j], tid); }
		k = i2;
		while (k <= j) { j -= k; k >>= 1; }
		j += k;
	}

	for (i = n, m = 0; i > 1; m ++, i /= 2);

	// Compute FFT
	c1 = -1.0; c2 = 0.0; l2 = 1;
	for (l = 0; l < m; l ++) {
		l1 = l2; l2 <<= 1;
		u.r = 1.0; u.i = 0.0;

		for (j = 0; j < l1; j ++) {
			for (i = j; i < n; i += l2) {
				i1 = i + l1;

				// t = u * r[i1]
				t.r = u.r * r[i1].r - u.i * r[i1].i;
				t.i = u.r * r[i1].i + u.i * r[i1].r;

				// r[i1] = r[i] - t
				r[i1].r = r[i].r - t.r;
				r[i1].i = r[i].i - t.i;

				// r[i] = r[i] + t
				r[i].r += t.r; r[i].i += t.i;
			}

			z = u.r * c1 - u.i * c2;
			u.i = u.r * c2 + u.i * c1;
			u.r = z;
		}

		c2 = sqrt((1.0 - c1) / 2.0);
		if (-1 == isign) { c2 = -c2; }
		c1 = sqrt((1.0 + c1) / 2.0);
	}

	// Scaling for inverse transform
	if (1 == isign) {		// inversed FFT
		for (i = 0; i < n; i ++) {
			r[i].r /= n;
			r[i].i /= n;
		}
	}
}


// fft whole function
//     for pthread
void* fft_2d(void* _tid) {
	int tid = *((int* )_tid);
	int row, col, p, q;

	printf("%d  - Thread %d started!\n", rank, tid);

	suspend(MATRIX_NONE);

	// #############################
	// ### fft-2d to A
	// #############################

	// row-wise fft-1d

	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row += NTHREAD) {
		c_fft1d_p(A[row], 512, -1, tid);
	}


	suspend(MATRIX_NONE);

	// tanspose matrix (in small blocks)

	for (p = 0; p < nproc; p ++) {
		matrix_transpose_p(A, rank * b_size, p * b_size, b_size, tid, NTHREAD);
	}


	suspend(MATRIX_COMM_A1);	// MPI Communication in this part.


	matrix_blockcopy_p(A, T, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size, tid, NTHREAD);


	suspend(MATRIX_NONE);

	// row-wise fft-1d


	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row += NTHREAD) {
		c_fft1d_p(T[row], 512, -1, tid);
	}


	suspend(MATRIX_NONE);

	// transpose matrix (in small blocks)

	for (p = 0; p < nproc; p ++) {
		matrix_transpose_p(T, rank * b_size, p * b_size, b_size, tid, NTHREAD);
	}


	suspend(MATRIX_COMM_A2);	// MPI Communication in this part.


	matrix_blockcopy_p(T, A, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size, tid, NTHREAD);


	suspend(MATRIX_NONE);

	// #############################
	// ### fft-2d to B
	// #############################

	// row-wise fft-1d

	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row += NTHREAD) {
		c_fft1d_p(B[row], 512, -1, tid);
	}


	suspend(MATRIX_NONE);

	// tanspose matrix (in small blocks)

	for (p = 0; p < nproc; p ++) {
		matrix_transpose_p(B, rank * b_size, p * b_size, b_size, tid, NTHREAD);
	}


	suspend(MATRIX_COMM_B1);	// MPI Communication in this part.


	matrix_blockcopy_p(B, T, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size, tid, NTHREAD);


	suspend(MATRIX_NONE);

	// row-wise fft-1d

	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row += NTHREAD) {
		c_fft1d_p(T[row], 512, -1, tid);
	}


	suspend(MATRIX_NONE);

	// transpose matrix (in small blocks)

	for (p = 0; p < nproc; p ++) {
		matrix_transpose_p(T, rank * b_size, p * b_size, b_size, tid, NTHREAD);
	}


	suspend(MATRIX_COMM_B2);	// MPI Communication in this part.


	matrix_blockcopy_p(T, B, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size, tid, NTHREAD);


	suspend(MATRIX_NONE);

	// #############################
	// ### mmpoint function
	// #############################


	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row ++) {
		for (col = 0; col < 512; col ++) {
			C[row][col].r = A[row][col].r * B[row][col].r - A[row][col].i * B[row][col].i;
			C[row][col].i = A[row][col].i * B[row][col].r + A[row][col].r * B[row][col].i;
		}
	}


	suspend(MATRIX_NONE);

	// #############################
	// ### inverse fft-2d
	// #############################

	// row-wise fft-1d

	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row += NTHREAD) {
		c_fft1d_p(C[row], 512, 1, tid);
	}


	suspend(MATRIX_NONE);

	// tanspose matrix (in small blocks)

	for (p = 0; p < nproc; p ++) {
		matrix_transpose_p(C, rank * b_size, p * b_size, b_size, tid, NTHREAD);
	}


	suspend(MATRIX_COMM_C1);	// MPI Communication in this part.


	matrix_blockcopy_p(C, T, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size, tid, NTHREAD);


	suspend(MATRIX_NONE);

	// row-wise fft-1d

	for (row = rank * b_size + tid; row < (rank + 1) * b_size; row += NTHREAD) {
		c_fft1d_p(T[row], 512, 1, tid);
	}


	suspend(MATRIX_NONE);

	// transpose matrix (in small blocks)

	for (p = 0; p < nproc; p ++) {
		matrix_transpose_p(T, rank * b_size, p * b_size, b_size, tid, NTHREAD);
	}


	suspend(MATRIX_COMM_C2);	// MPI Communication in this part.


	matrix_blockcopy_p(T, C, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size, tid, NTHREAD);


	suspend(GATHER_DATA);
}

// main part
void test1() {
	if (0 == rank) { printf("## Start test 1 ##\n"); }
	if (IM_FILE_SUCCESS != ReadIm("1_im1", A)) {
		printf("%d  - Error reading file 1_im1\n", rank);
		return;
	}
	if (IM_FILE_SUCCESS != ReadIm("1_im2", B)) {
		printf("%d  - Error reading file 1_im2\n", rank);
		return;
	}

	tclear();

	double t_time = MPI_Wtime();

	int ii;
	for (ii = 1; ii < NTHREAD; ii ++) {
		int *r_tid = malloc(sizeof(int)); *r_tid = ii;
		pthread_create(&pool[ii - 1], NULL, fft_2d, r_tid);
	}

	tic();
	int *tid = malloc(sizeof(int)); *tid = 0;
	*fft_2d(tid);

	for (ii = 0; ii < NTHREAD - 1; ii ++) {
		pthread_join(pool[ii], NULL);
	}

	t_time = MPI_Wtime() - t_time;

	if (0 == rank) {
		printf("Writing results.");
		char outfile[100];
		sprintf(outfile, "out_1_c_%dproc", nproc);
		if (IM_FILE_SUCCESS != WriteIm(outfile, C)) {
			printf("Error writing file %s\n", outfile);
		}

		printf("The total time cost is %f ms. (Test 1)\n", t_time * 1000);

		printf("The total communication time is %f ms, and computation time is %f ms\n",
			t_comm * 1000, t_comp * 1000);
	}
}

void test2() {
	if (0 == rank) { printf("## Start test 1 ##\n"); }
	if (IM_FILE_SUCCESS != ReadIm("2_im1", A)) {
		printf("%d  - Error reading file 2_im1\n", rank);
		return;
	}
	if (IM_FILE_SUCCESS != ReadIm("2_im2", B)) {
		printf("%d  - Error reading file 2_im2\n", rank);
		return;
	}

	tclear();

	double t_time = MPI_Wtime();

	int ii;
	for (ii = 1; ii < NTHREAD; ii ++) {
		int *r_tid = malloc(sizeof(int)); *r_tid = ii;
		pthread_create(&pool[ii - 1], NULL, fft_2d, r_tid);
	}

	tic();
	int *tid = malloc(sizeof(int)); *tid = 0;
	*fft_2d(tid);

	for (ii = 0; ii < NTHREAD - 1; ii ++) {
		pthread_join(pool[ii], NULL);
	}

	t_time = MPI_Wtime() - t_time;

	if (0 == rank) {
		printf("Writing results.");
		char outfile[100];
		sprintf(outfile, "out_2_c_%dproc", nproc);
		if (IM_FILE_SUCCESS != WriteIm(outfile, C)) {
			printf("Error writing file %s\n", outfile);
		}

		printf("The total time cost is %f ms. (Test 2)\n", t_time * 1000);

		printf("The total communication time is %f ms, and computation time is %f ms. (Test 2)\n",
			t_comm * 1000, t_comp * 1000);
	}
}

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Datatype_Init();

	#ifdef DEBUG
	debug_count = 1;
	#endif

	// ## test 1 ## //
	test1();
	test2();

	MPI_Finalize();
	return 0;
}
