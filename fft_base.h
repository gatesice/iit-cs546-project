// ============================================
//
//           CS542 Project
//
// --------------------------------------------
// filename : fft_base.h
// author   : Gates Wong
// date     : 2014-05-06
//
// This project entails implementing
// two-dimensional convolution on parallel
// computers using different parallelization
// techniques and models.
//
// This file is the origin version of FFT
//
// Origin version is designed for Linear
// processing models. This is non-parallel
// program.
//
// ============================================

#pragma once

typedef struct { float r; float i; } complex;

// Swap two complex numbers
static complex __ctmp;
#define C_SWAP(A, B) { __ctmp = (A); (A) = (B); (B) = __ctmp; }

// c_fft1d() function
// parameters:
//      complex *r : real and imaginary arrays of n = 2^m points
//      int isign : -1 gives forward transform
//                  1 gives inverse transform
void c_fft1d(complex *r, int n, int isign);

// matrix_transpose() function
// parameters:
//     complex* data : the matrix used for transform
//     rowstart : row index of start point
//     colstart : col index of start point
//     size : the size of transpose part
void matrix_transpose(complex (* data)[512], int rowstart, int colstart, int size);

// matrix_blockcopy() function
//     copy a block of matrix
// parameters:
//     src: the source matrix
//     dst: the destination matrix
//     srow, scol: where source matrix starts
//     trow, tcol: where destination matrix starts
//     size: how large is the block
void matrix_blockcopy(complex (* src)[512], complex (*dst)[512], int srow, int scol,
	int trow, int tcol, int size);

// file read/write return status
#define IM_FILE_SUCCESS 0
#define IM_FILE_CANNOT_OPEN 1
#define IM_FILE_CONTENT_ERROR 2

// ReadIm() function
// parameters:
//     filepath - The full path of input file.
//     data - the pointer where data store at.
// return:
//     values in FileOperationResults.
int ReadIm(char*, complex (*data)[512]);

// WriteIm() function
// parameters:
//     filepath - The full path of input file.
//     data - the pointer where data store at.
// return:
//     values in FileOperationResults.
int WriteIm(char*, complex (*data)[512]);
