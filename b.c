// ============================================
//
//           CS542 Project
//
// --------------------------------------------
// filename : b.c
// author   : Gates Wong
// date     : 2014-05-06
//
// This project entails implementing
// two-dimensional convolution on parallel
// computers using different parallelization
// techniques and models.
//
// This file is the origin version of FFT
//
// FFT using MPI collection.
//
// ============================================

#include "fft_base.h"

#include <stdio.h>
#include <mpi.h>

// timer utility
double t_comm, t_comp, t_last;
void tclear() { t_comm = t_comp = 0.0; }
void tic() { t_last = MPI_Wtime(); }
void tac(double* t) { *t += MPI_Wtime() - t_last; }

// global MPI Variables
int rank, nproc;	// current rank and number of processes
int b_size;
MPI_Datatype FFT_COMPLEX, FFT_BLOCK;
void MPI_Datatype_Init() {
	b_size = 512/nproc;
	MPI_Type_contiguous(2, MPI_FLOAT, &FFT_COMPLEX);
	MPI_Type_commit(&FFT_COMPLEX);			// MPI type for complex
	MPI_Type_vector(b_size, b_size, 512, FFT_COMPLEX, &FFT_BLOCK);
	MPI_Type_commit(&FFT_BLOCK);			// MPI type for block growing vertically.
}

// vectors
complex A[512][512], B[512][512], C[512][512], T[512][512];

// matrix_buffer() function
//     copy part of matrix into buffer
// parameters:
//     direction: if 0, copy from A to buffer; else copy from buffer to A.
void matrix_buffer(complex (*data)[512], complex *buffer, int direction) {
	int row = 0, col = 0, offset = 0;
	for (row = 0, col = 0, offset = 0; offset < 512 * b_size;
		offset += b_size,
		row = (row + 1) % b_size,
		col = offset / (b_size * b_size)) {
		if (0 == direction) {
			memcpy(&buffer[offset], &data[row + rank * b_size][col * b_size], sizeof(complex) * b_size);
		} else {
			memcpy(&data[row + rank * b_size][col * b_size], &buffer[offset], sizeof(complex) * b_size);
		}
	}
}

// fft_2d function
void fft_2d(complex (*A)[512], int isign) {
	int row, col, p, q, rowt, colt;
	MPI_Status status;

	// ### for 1 processor : do all works withot communications
	if (1 == nproc) {
		tic();
		for (row = 0; row < 512; row ++) {
			c_fft1d(A[row], 512, isign);
		}

		matrix_transpose(A, 0, 0, 512);

		for (row = 0; row < 512; row ++) {
			c_fft1d(A[row], 512, isign);
		}

		matrix_transpose(A, 0, 0, 512);

		tac(&t_comp);
		return;
	}

	// ### for more processors:
	tic();
	for (row = rank * b_size; row < (rank + 1) * b_size; row ++) {
		c_fft1d(A[row], 512, isign);
	}

	// Local computation: transpose small blocks.
	for (p = 0; p < nproc; p ++) {
		matrix_transpose(A, rank * b_size, p * b_size, b_size);
	}
	tac(&t_comp);



	// Communication
	complex *T1 = (complex*) malloc(sizeof(complex) * 512 * b_size),
		*T2 = (complex*) malloc(sizeof(complex) * 512 * b_size);
	tic();
	matrix_buffer(A, T1, 0);
	tac(&t_comp);

	tic();
	MPI_Alltoall(T1, b_size * b_size, FFT_COMPLEX,
		T2, b_size * b_size, FFT_COMPLEX, MPI_COMM_WORLD);
	tac(&t_comm);

	tic();
	matrix_buffer(T, T2, 1);

	// Local computation: 1D-FFT
	for (row = rank * b_size; row < (rank + 1) * b_size; row ++) {
		c_fft1d(T[row], 512, isign);
	}

	// Local computation: transpose small blocks.
	for (p = 0; p < nproc; p ++) {
		matrix_transpose(T, rank * b_size, p * b_size, b_size);
	}

	// Local computation: transpose small blocks
	matrix_buffer(T, T1, 0);
	tac(&t_comp);

	// Communication : gather data back
	tic();
	MPI_Alltoall(T1, b_size * b_size, FFT_COMPLEX,
		T2, b_size * b_size, FFT_COMPLEX, MPI_COMM_WORLD);
	tac(&t_comm);

	tic();
	matrix_buffer(A, T2, 1);
	tac(&t_comp);

	free(T1); free(T2);
}

// mmpoint function
void mmpoint(complex (*A)[512], complex (*B)[512], complex (*C)[512]) {
	int row, col;
	tic();
	for (row = rank * b_size; row < (rank + 1) * b_size; row ++) {
		for (col = 0; col < 512; col ++) {
			C[row][col].r = A[row][col].r * B[row][col].r - A[row][col].i * B[row][col].i;
			C[row][col].i = A[row][col].i * B[row][col].r + A[row][col].r * B[row][col].i;
		}
	}
	tac(&t_comp);
}

// gather all data to process 0
void gather(complex (*A)[512]) {
	if (nproc == 1) { return; }
	printf("%d  - gathering data.\n", rank);

	// copy send data to T as sendbuffer.
	memcpy(T, A[rank * b_size], sizeof(complex) * 512 * b_size);

	MPI_Gather(T, 512 * b_size, FFT_COMPLEX,
		A[rank * b_size], 512 * b_size, FFT_COMPLEX,
		0, MPI_COMM_WORLD);
}

// main part
void test1() {
	if (0 == rank) {
		printf("## Start test 1 ##\n");
	}

	if (IM_FILE_SUCCESS != ReadIm("1_im1", A)) {
		printf("%d  - Error reading file 1_m1\n", rank);
		return;
	}

	if (IM_FILE_SUCCESS != ReadIm("1_im2", B)) {
		printf("%d  - Error reading file 1_m2\n", rank);
		return;
	}

	if (0 == rank) {
		printf("## Start FFT ##\n");
	}

	tclear();
	double t_time = MPI_Wtime();

	fft_2d(A, -1);

	fft_2d(B, -1);

	mmpoint(A, B, C);

	fft_2d(C, 1);

	gather(C);

	t_time = MPI_Wtime() - t_time;

	if (0 == rank) {
		printf("Writing results.");
		char outfile[100];
		sprintf(outfile, "out_1_b_%dproc", nproc);
		if (IM_FILE_SUCCESS != WriteIm(outfile, C)) {
			printf("Error writing file %s\n", outfile);
		}

		printf("The total time cost is %f ms. (Test 1)\n", t_time * 1000);
		printf("The total communication time is %f ms, and computation time is %f ms. (Time 1)\n",
			t_comm * 1000, t_comp * 1000);
	}
}

void test2() {
	if (0 == rank) {
		printf("## Start test 2 ##\n");
	}

	if (IM_FILE_SUCCESS != ReadIm("2_im1", A)) {
		printf("%d  - Error reading file 2_m1\n", rank);
		return;
	}

	if (IM_FILE_SUCCESS != ReadIm("2_im2", B)) {
		printf("%d  - Error reading file 2_m2\n", rank);
		return;
	}

	if (0 == rank) {
		printf("## Start FFT ##\n");
	}
	tclear();

	double t_time = MPI_Wtime();

	fft_2d(A, -1);

	fft_2d(B, -1);

	mmpoint(A, B, C);

	fft_2d(C, 1);

	gather(C);

	t_time = MPI_Wtime() - t_time;

	if (0 == rank) {
		printf("Writing results.");
		char outfile[100];
		sprintf(outfile, "out_2_b_%dproc", nproc);
		if (IM_FILE_SUCCESS != WriteIm(outfile, C)) {
			printf("Error writing file %s\n", outfile);
		}

		printf("The total time cost is %f ms. (Test 2)\n", t_time * 1000);
		printf("The total communication time is %f ms, and computation time is %f ms. (Test 2)\n",
			t_comm * 1000, t_comp * 1000);
	}
}

int main(int argc, char **argv) {
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	MPI_Datatype_Init();

	// ## test 1 ## //
	test1();
	test2();

	MPI_Finalize();
	return 0;
}
