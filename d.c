// ============================================
//
//           CS542 Project
//
// --------------------------------------------
// filename : d.c
// author   : Gates Wong
// date     : 2014-05-07
//
// This project entails implementing
// two-dimensional convolution on parallel
// computers using different parallelization
// techniques and models.
//
// This file is the origin version of FFT
//
// FFT using MPI, task and data parallel.
//
// ============================================

#include "fft_base.h"

#include <stdio.h>
#include <mpi.h>

// timer utility
double t_comm, t_comp, t_last;
void tclear() { t_comm = t_comp = 0.0; }
void tic() { t_last = MPI_Wtime(); }
void tac(double* t) { *t += MPI_Wtime() - t_last; }

// global MPI Variables
int rank, nproc, task;	// current rank and number of processes, in TASK LEVEL
int global_rank, global_nproc;
int b_size;
MPI_Datatype FFT_COMPLEX, FFT_BLOCK;
MPI_Comm FFT_COMM;
void MPI_Datatype_Init() {
	b_size = 512/nproc;
	MPI_Type_contiguous(2, MPI_FLOAT, &FFT_COMPLEX);
	MPI_Type_commit(&FFT_COMPLEX);			// MPI type for complex
	MPI_Type_vector(b_size, b_size, 512, FFT_COMPLEX, &FFT_BLOCK);
	MPI_Type_commit(&FFT_BLOCK);			// MPI type for block growing vertically.
}

// tasks
#define TASK_FFT2D_A 0
#define TASK_FFT2D_B 1
#define TASK_MMPOINT 2
#define TASK_IFFT2D 3

// vectors
complex A[512][512], B[512][512], C[512][512], T[512][512];

// fft_2d function
void fft_2d(complex (*A)[512], int isign) {
	int row, col, p, q;
	MPI_Status status;

	// ### for 1 processor : do all works without communications
	if (1 == nproc) {
		// do all works withour MPI communications.
		tic();
		for (row = 0; row < 512; row ++) {
			c_fft1d(A[row], 512, isign);
		}

		matrix_transpose(A, 0, 0, 512);

		for(row = 0; row < 512; row ++) {
			c_fft1d(A[row], 512, isign);
		}

		matrix_transpose(A, 0, 0, 512);

		tac(&t_comp);
		return;
	}

	// ### for more processors :
	tic();	// Local computation, 1D-FFT
	for (row = rank * b_size; row < (rank + 1) * b_size; row ++) {
		c_fft1d(A[row], 512, isign);
	}

	// Local computation: transpose small blocks.
	for (p = 0; p < nproc; p ++) {
		matrix_transpose(A, rank * b_size, p * b_size, b_size);
	}
	tac(&t_comp);

	// Send and recv blocks between processes.
	tic();
	for (p = 1; p < nproc; p ++) {
		// part 1: communication between processes
		int s = (rank + p) % nproc,
			r = (rank - p + nproc) % nproc;

		MPI_Sendrecv(&A[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
			&T[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
			FFT_COMM, &status);
	}
	tac(&t_comm);
	// part 2: copy locally
	matrix_blockcopy(A, T, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size);

	// Local computation: 1D-FFT
	tic();
	for (row = rank * b_size; row < (rank + 1) * b_size; row ++) {
		c_fft1d(T[row], 512, isign);
	}

	// Local computation: transpose small blocks.
	for (p = 0; p < nproc; p ++) {
		matrix_transpose(T, rank * b_size, p * b_size, b_size);
	}
	tac(&t_comp);

	// Send and recv blocks between processes.
	tic();
	for (p = 1; p < nproc; p ++) {
		int s = (rank + p) % nproc,
			r = (rank - p + nproc) % nproc;

		MPI_Sendrecv(&T[rank * b_size][s * b_size], 1, FFT_BLOCK, s, rank,
			&A[rank * b_size][r * b_size], 1, FFT_BLOCK, r, r,
			FFT_COMM, &status);
	}
	tac(&t_comm);
	matrix_blockcopy(T, A, rank * b_size, rank * b_size,
		rank * b_size, rank * b_size, b_size);
}

// mmpoint function
void mmpoint(complex (*A)[512], complex (*B)[512], complex (*C)[512]) {
	int row, col;
	tic();
	for (row = rank * b_size; row < (rank + 1) * b_size; row ++) {
		for (col = 0; col < 512; col ++) {
			C[row][col].r = A[row][col].r * B[row][col].r - A[row][col].i * B[row][col].i;
			C[row][col].i = A[row][col].i * B[row][col].r + A[row][col].r * B[row][col].i;
		}
	}
	tac(&t_comp);
}

// gather all data to process 0
void gather(complex (*A)[512]) {
	if (nproc == 1) { return; }
	printf("%d  - gathering data.\n", rank);

	MPI_Status status;
	if (0 == rank) {
		tic();
		int p, q;
		for (p = 1; p < nproc; p ++) {
			// receive data from processor:
			MPI_Recv(A[p * b_size], 512 * b_size, FFT_COMPLEX, p, p, FFT_COMM, &status);
		}
		tac(&t_comm);
	} else {
		tic();
		MPI_Send(A[rank * b_size], 512 * b_size, FFT_COMPLEX, 0, rank, FFT_COMM);
		tac(&t_comm);
	}
}

// main part
void test1() {
	if (0 == global_rank) {
		printf("## Start test 1 ##\n");
	}

	double t_iotime = MPI_Wtime();

	if (task == TASK_FFT2D_A && IM_FILE_SUCCESS != ReadIm("1_im1", A)) {
		printf("%d  - Error reading file 1_m1\n", global_rank);
		return;
	}

	if (task == TASK_FFT2D_B && IM_FILE_SUCCESS != ReadIm("1_im2", B)) {
		printf("%d  - Error reading file 1_m2\n", global_rank);
		return;
	}

	t_iotime = MPI_Wtime() - t_iotime;

	if (task == TASK_FFT2D_A || task == TASK_FFT2D_B) {
		printf("%d  - Local rank %d takes %f ms to read files.", global_rank, rank, t_iotime * 1000);
	}

	int p; MPI_Status status;

	double t_time = MPI_Wtime();
	tclear();

	// Task parallel
	switch (task) {

	case TASK_FFT2D_A:
		fft_2d(A, -1);
		tic();
		MPI_Send(&A[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_MMPOINT * nproc, rank, MPI_COMM_WORLD);
		tac(&t_comm);
		break;

	case TASK_FFT2D_B:
		fft_2d(B, -1);
		tic();
		MPI_Send(&B[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_MMPOINT * nproc, rank, MPI_COMM_WORLD);
		tac(&t_comm);
		break;

	case TASK_MMPOINT:
		// receive data from TASK_FFT2D_*
		tic();
		MPI_Recv(&A[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_FFT2D_A * nproc, rank, MPI_COMM_WORLD, &status);
		MPI_Recv(&B[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_FFT2D_B * nproc, rank, MPI_COMM_WORLD, &status);
		tac(&t_comm);
		mmpoint(A, B, C);
		tic();
		MPI_Send(&C[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_IFFT2D * nproc, rank, MPI_COMM_WORLD);
		tac(&t_comm);
		break;

	case TASK_IFFT2D:
		tic();
		MPI_Recv(&C[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_MMPOINT * nproc, rank, MPI_COMM_WORLD, &status);
		tac(&t_comm);
		fft_2d(C, 1);
		gather(C);
		break;
	}

	t_time = MPI_Wtime() - t_time;
	printf("%d  - The total running time is %f ms. (Test 1)\n", global_rank, t_time * 1000);
	printf("%d  - The total communication time is %f ms, and computation time is %f ms. (Test 1)\n",
		global_rank, t_comm * 1000, t_comp * 1000);


	if (0 == rank && nproc * TASK_IFFT2D + rank == global_rank) {
		printf("Writing results.\n");
		char outfile[100];
		sprintf(outfile, "out_1_d_%dproc", global_nproc);
		if (IM_FILE_SUCCESS != WriteIm(outfile, C)) {
			printf("Error writing file %s\n", outfile);
		}
	}
}

void test2() {
	if (0 == global_rank) {
		printf("## Start test 2 ##\n");
	}

	double t_iotime = MPI_Wtime();

	if (task == TASK_FFT2D_A && IM_FILE_SUCCESS != ReadIm("2_im1", A)) {
		printf("%d  - Error reading file 2_m1\n", global_rank);
		return;
	}

	if (task == TASK_FFT2D_B && IM_FILE_SUCCESS != ReadIm("2_im2", B)) {
		printf("%d  - Error reading file 2_m2\n", global_rank);
		return;
	}

	t_iotime = MPI_Wtime() - t_iotime;

	if (task == TASK_FFT2D_A || task == TASK_FFT2D_B) {
		printf("%d  - Local rank %d takes %f ms to read files.", global_rank, rank, t_iotime * 1000);
	}

	int p; MPI_Status status;

	double t_time = MPI_Wtime();
	tclear();

	// Task parallel
	switch (task) {

	case TASK_FFT2D_A:
		fft_2d(A, -1);
		tic();
		MPI_Send(&A[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_MMPOINT * nproc, rank, MPI_COMM_WORLD);
		tac(&t_comm);
		break;

	case TASK_FFT2D_B:
		fft_2d(B, -1);
		tic();
		MPI_Send(&B[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_MMPOINT * nproc, rank, MPI_COMM_WORLD);
		tac(&t_comm);
		break;

	case TASK_MMPOINT:
		// receive data from TASK_FFT2D_*
		tic();
		MPI_Recv(&A[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_FFT2D_A * nproc, rank, MPI_COMM_WORLD, &status);
		MPI_Recv(&B[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_FFT2D_B * nproc, rank, MPI_COMM_WORLD, &status);
		tac(&t_comm);
		mmpoint(A, B, C);
		tic();
		MPI_Send(&C[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_IFFT2D * nproc, rank, MPI_COMM_WORLD);
		tac(&t_comm);
		break;

	case TASK_IFFT2D:
		tic();
		MPI_Recv(&C[rank * b_size][0], 512 * b_size, FFT_COMPLEX,
			rank + TASK_MMPOINT * nproc, rank, MPI_COMM_WORLD, &status);
		tac(&t_comm);
		fft_2d(C, 1);
		gather(C);
		break;
	}

	t_time = MPI_Wtime() - t_time;
	printf("%d  - The total running time is %f ms. (Test 2)\n", global_rank, t_time * 1000);
	printf("%d  - The total communication time is %f ms, and computation time is %f ms. (Test 2)\n",
		global_rank, t_comm * 1000, t_comp * 1000);


	if (0 == rank && nproc * TASK_IFFT2D + rank == global_rank) {
		printf("Writing results.\n");
		char outfile[100];
		sprintf(outfile, "out_2_d_%dproc", global_nproc);
		if (IM_FILE_SUCCESS != WriteIm(outfile, C)) {
			printf("Error writing file %s\n", outfile);
		}
	}
}

int main(int argc, char **argv) {
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &global_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &global_nproc);
	task = global_rank / 2;
	MPI_Comm_split(MPI_COMM_WORLD, task, global_rank, &FFT_COMM);
	MPI_Comm_rank(FFT_COMM, &rank);
	MPI_Comm_size(FFT_COMM, &nproc);
	MPI_Datatype_Init();

	// ## test 1 ## //
	test1();

	// ## test 2 ## //
	test2();

	MPI_Finalize();
	return 0;
}
